package Homework_OnlineBasket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class basket_online_store implements basket {

    public HashMap<String, Double> basket = new HashMap<>();

    @Override
    public void addProduct(String product, double quantity) {
        basket.put(product, quantity);
        System.out.println("Добавлено " + quantity + " " + product + " в корзину!");
    }


    @Override
    public void removeProduct(String product) {
        if (basket.containsKey(product)) {
            basket.remove(product);
            System.out.println(product + " удален из корзины!");
        } else {
            System.out.println("Нельзя удалить " + product + ", нет в корзине!");
        }
    }

    @Override
    public void clear() {
        basket.clear();
        System.out.println("Корзина очищена!");
    }

    @Override
    public List<String> getProducts() {
        List<String> products = new ArrayList<>();
        if (!basket.isEmpty()) {
            products.addAll(basket.keySet());
        }
        return products;
    }

    @Override
    public void updateProductQuantity(String product, double quantity) {
        if (basket.containsKey(product)) {
            basket.replace(product, quantity);
        } else {
            System.out.println("Нельзя изменить кол-во " + product + ", нет в корзине!");
        }
    }

    @Override
    public double getProductQuantity(String product) {
        double quantity = 0;
        if (basket.containsKey(product)) {
            quantity = (double) basket.get(product);
        } else {
            System.out.println( product + " нет в корзине!");
        };
        return quantity;
    }
    @Override
    public void outputbasket () {
        if (!basket.isEmpty()) {
            for (Map.Entry<String, Double> i : basket.entrySet()) {
                System.out.printf("Наименование: %s  Количество: %.1f \n", i.getKey(), i.getValue());
            }
        } else {
            System.out.println( "---Корзина пуста!---");
        }
    }
}


