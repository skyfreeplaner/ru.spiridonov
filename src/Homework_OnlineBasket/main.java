package Homework_OnlineBasket;

import java.util.HashMap;
import java.util.logging.Logger;

public class main {

    public static void main(String[] args) {
        final Logger logger = Logger.getLogger("basketlog");
        basket_online_store basket1 = new basket_online_store();
        try {
            basket1.addProduct("Молоко", 2);
            basket1.addProduct("Сыр", 1.3);
            basket1.addProduct("Творог", 4.7);
            basket1.outputbasket();
            basket1.addProduct("Творог", 1.2);
            basket1.removeProduct("Сыр");
            basket1.outputbasket();
            basket1.removeProduct("Сыр");
            basket1.updateProductQuantity("Молоко", 3);
            basket1.updateProductQuantity("Сыр", 3.2);
            System.out.println(basket1.getProducts());
            basket1.clear();
            basket1.outputbasket();


        } catch (Exception e ){
            logger.severe(e.getMessage());
            //System.out.println("Ошибка! Попробуйте еще раз!") ;
          }

    }
}
