package Homework_OnlineBasket;

import java.util.List;

public interface basket {
    void addProduct(String product, double quantity);

    void removeProduct(String product);

    void updateProductQuantity(String product, double quantity);

    void clear();

    List<String> getProducts();

    double getProductQuantity(String product);
    void outputbasket();
}
