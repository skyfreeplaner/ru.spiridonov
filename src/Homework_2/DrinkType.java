package Homework_2;

public enum DrinkType {
    COLD("Холодные напитки"),
    HOT("Горячие напитки");

    public String title;

    DrinkType(String title) {
        this.title = title;
    }

    public String getTitle() { return title; }
}


