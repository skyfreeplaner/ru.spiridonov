package Homework_2;
import Homework_2.DrinkType;

public class SaleDrink implements Drink {

    public String title;
    public double price;
    public DrinkType type;

    public SaleDrink(String title, double price, DrinkType type) {
        this.title = title;
        this.price = price;
        this.type = type;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public DrinkType getType() {
        return type;
    }


}
