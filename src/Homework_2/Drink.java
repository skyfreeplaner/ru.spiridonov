package Homework_2;

public interface Drink {
        String getTitle();
        double getPrice();
        DrinkType getType();

}
