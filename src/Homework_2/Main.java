import Homework_2.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Drink[] Drinks = new SaleDrink[]{
                new SaleDrink("Чай", 50, DrinkType.HOT),
                new SaleDrink("Кофе", 100, DrinkType.HOT),
                new SaleDrink("Какао", 120, DrinkType.HOT),
                new SaleDrink("Пепси", 60, DrinkType.COLD),
                new SaleDrink("Спрайт", 70, DrinkType.COLD),
                new SaleDrink("Фанта", 80, DrinkType.COLD),
        };


        VendingMachine vm = new VendingMachine(Drinks);
        Scanner input = new Scanner(System.in);

        try{
            System.out.print("Выберите тип напитка (COLD/HOT): ");
            String type = input.nextLine();
            DrinkType Drinktype = DrinkType.valueOf(type);
            vm.out(type);
            System.out.println("Введите номер напитка:__ ");
            int numberDrink = input.nextInt();
            System.out.println("Внесите деньги -  ");
            double addPay = input.nextDouble();
            vm.payMoney(addPay);
            vm.giveMeADrink(numberDrink, type);
        } catch (InputMismatchException | IllegalArgumentException ex ) {
            System.out.println("Неверная команда!");
        }
        System.out.println();

    }
}




/* public class Main {
    public static void main (String[] args) throws IOException {


          System.out.println("Сегодня в продаже ICE CREAM ! \nВ продаже имеются напитки:  ");
          System.out.println("Введите номер напитка: ");

         BufferedReader input_1 = new BufferedReader(new InputStreamReader(System.in));
         String inputString;
         inputString = input_1.readLine();
         System.out.println("Вы выбрали: " + SaleDrink.valueOf(inputString) );

         }


     }




         BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
         String number = reader.readLine();
         int newnumber = Integer.getInteger(number);
         System.out.println("Вы выбрали: "+ newnumber);
         System.out.println("Введите деньги:");
         BufferedReader reader1 = new BufferedReader(new InputStreamReader(System.in));
         String money = reader1.readLine();
         double newmoney = Double.parseDouble(money);

     }


 }
















 /*    public enum ColdDrink {
        CD1("Pepsi", 70, 1), CD2("Fanta", 60, 2), CD3("Sprite", 50, 3);

        String a;
        double p;
        int n;

        ColdDrink(String a, double p, int n) {
            this.a = a;
            this.p = p;
            this.n = n;
        }

    }
    public enum DrinkType {
        HD1("Tea", 100, 4), HD2("Coffe", 150, 5), HD3("Kakao", 160, 6);

        String a;
        double p;
        int n;

        DrinkType(String a, double p, int n) {
            this.a = a;
            this.p = p;
            this.n = n;
        }
    }

    public static void main(String[] args) {
        System.out.println(" Напиток " + " Стоимость " + "Номер");
        System.out.println(" " + ColdDrink.CD1.a + "     " + ColdDrink.CD1.p + "      " + ColdDrink.CD1.n );
        System.out.println(" " + ColdDrink.CD2.a + "     " + ColdDrink.CD2.p + "      " + ColdDrink.CD2.n );
        System.out.println(" " + ColdDrink.CD3.a + "    " + ColdDrink.CD3.p + "      " + ColdDrink.CD3.n );
        System.out.println(" " + DrinkType.HD1.a + "       " + DrinkType.HD1.p + "     " + DrinkType.HD1.n );
        System.out.println(" " + DrinkType.HD2.a + "     " + DrinkType.HD2.p + "     " + DrinkType.HD2.n );
        System.out.println(" " + DrinkType.HD3.a + "     " + DrinkType.HD3.p + "     " + DrinkType.HD3.n );





    }



}
*/


