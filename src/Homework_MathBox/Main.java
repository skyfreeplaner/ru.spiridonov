package Homework_MathBox;

import java.lang.reflect.Proxy;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {

        final Logger logger = Logger.getLogger("MathBox");
        MathBox mathBox = new MathBox();
       try{
           mathBox.add(2);
           mathBox.add(7);
           mathBox.add(12);
           mathBox.add(7);
           mathBox.add(8);
           mathBox.dele(7);
           mathBox.dele(11);
           logger.info("Максимальное значение: " + mathBox.getMax());
           logger.info("Минимальное значение: " + mathBox.getMin());
           logger.info("Вывод значений: " + mathBox.getIntList() );
           logger.info("Сумма значений: " + mathBox.getTotal());
           logger.info("Среднее значение: " + mathBox.getAverage());
           logger.info("Вывод значений после умножения: " + mathBox.getMultiplied(2));
           logger.info("Все значения: " + mathBox.getMap().toString());

           BoxProxy boxProxy = new BoxProxy(mathBox);
           Box box = (Box) Proxy.newProxyInstance(
                   BoxProxy.class.getClassLoader(),
                   new Class[]{Box.class},
                   boxProxy);
           System.out.println();

           box.LogData();

           box.ClearData();


      }catch (Exception e) {
            logger.severe(e.getMessage());
        }

    }
}
