package Homework_MathBox;

import java.lang.reflect.Proxy;
import java.util.*;
import java.util.stream.IntStream;

public class MathBox<sum> implements Box{
    List<Integer> intList = new ArrayList<>();

    @Override
    public void add(int value) {
        if (intList.contains(value)) {
            System.out.println("Введенное значение уже присутствует в списке");
        } else { intList.add(value); };
    };
    @Override
    public void dele(int value) {
        if (intList.contains(value)) {
          intList.remove(Integer.valueOf(value));
        } else { System.out.println("Введенное значение отсутствует в списке"); }
    };
    @Override
    public int getTotal() {
        int sum = 0;
        for (int i: intList)
            sum = sum + i;
        return  sum;
    };
    @Override
    public List<Integer> getMultiplied(int multiplicator) {
            List<Integer> intmultiList = new ArrayList<>();
            for (int i : intList)
                intmultiList.add(i * multiplicator);
            intList = intmultiList;
            return intList;
    }
    @Override
    public Map<Integer, String> getMap() {
        Map<Integer,String> intMap = new HashMap<>();
        for (int i : intList) {
            intMap.put(i, String.valueOf(i));
        }return intMap;
    }
    @Override
    public int getMax() {
        return Collections.max(intList);
    };
    @Override
    public int getMin() {
        return Collections.min(intList);
    };
    @Override
    public double getAverage() {
        return (getTotal()/intList.size() );
    }
    @Override
    public void clear() {
        intList.clear();
    }
    @Override
    public List<Integer> getIntList(){
        return intList;
    }
    @Override
    @ClearData
    public void ClearData() {
        System.out.println("Метод, после которого проиходит очистка коллекции");
    }

    @Override
    @LogData
    public void LogData() {
        System.out.println("Метод, до и после которого происходит вывод коллекции");
    }
}




