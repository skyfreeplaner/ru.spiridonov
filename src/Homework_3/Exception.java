package Homework_3;

import java.util.ArrayList;
import java.util.Scanner;

public class Exception {

    public void NPE() {
      try {
          System.out.println("Введите Ваше имя: ");
          Scanner scanner = new Scanner(System.in);
          String Name= scanner.next((String) null);
          System.out.println("Введите Ваш возраст: ");
          int Age = scanner.nextInt();;
          System.out.println("Имя" + Name + "Возраст: " + Age);
      } catch (NullPointerException e){
          System.out.println("\nНекорректный ввод данных! \nПопробуйте еще раз! ");
        }
    }
    public void AIO(){
       try{
            String[] people = new String[10];
            people[0] = "Васильев";
            people[1] = "Смирнов";
            people[11] = "Иванов";
            System.out.println("\nФамилия - " + people[11]);
       } catch (ArrayIndexOutOfBoundsException e){
           System.out.println("\nВыбран несуществующий работник! \nПовторите ввод!");
          }

    }

    public void MyExp()  {
        try {
            short f = 1;
            for (int i = 1; ; i++) {
                f = (short) (f * i);
                if (f > 32767 || f < -32767) throw new Throwable();
                System.out.println(f);
            }
        }catch (Throwable e) {System.out.println("\nПревышено значение типа переменной! " ); }
    }

}
