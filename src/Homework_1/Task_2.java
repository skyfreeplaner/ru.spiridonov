package Homework_1;

import java.util.Scanner;
import java.util.Arrays;

public class Task_2 {
    public static void main(String[] args) {
        // Создаём объект Scanner
        Scanner input = new Scanner(System.in);
        // Запрос пользователю на ввод числа
        System.out.print("Введите число 1: ");
        int number1 = input.nextInt();
        System.out.print("Введите число 2: ");
        int number2 = input.nextInt();
        System.out.print("Введите число 3: ");
        int number3 = input.nextInt();
        System.out.print("Введите число 4: ");
        int number4 = input.nextInt();
        // Сортируем введенные числа
        int[] array = {number1, number2, number3, number4};
        Arrays.sort(array);
        System.out.println(array[0]);

    }
}
