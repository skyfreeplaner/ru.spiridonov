package Homework_1;

import java.util.Scanner;
public class Task_3 {
    public static void main(String[] args) {
        // Создаём объект Scanner
        Scanner input = new Scanner(System.in);
        // Запрос пользователю на ввод ограничения порога вывода таблицы умножения
        System.out.print("Введите порог вывода таблицы умноения: ");
        int number1 = input.nextInt();
        for(int a = 1; a <= number1; a++){
            for (int b = 1; b <= number1; b++){
                System.out.println( a + "*" + b + "=" + a * b + " " );
            }
        }

    }

}
