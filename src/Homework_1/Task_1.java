package Homework_1;

import java.util.Scanner;

public class Task_1 {

    public static void main(String[] args) {
        // Создаём объект Scanner
        Scanner input = new Scanner(System.in);
        // Запрос пользователю на ввод объема топлива
        System.out.print("Введите объем топлива: ");
        double volume = input.nextDouble();
        // Вычисление стоимости топлива
        //double area = volume * 43;
        // Отображение результата
        System.out.println("Стоимость топлива объемом " + volume + " литров равна " + volume * 43);
    }
}
